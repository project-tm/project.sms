<?php

namespace Project\Sms;

class Send {

    static public function sms($phone, $text) {
        $params = array(
            'login' => Config::LOGIN,
            'phone' => $phone,
            'text' => $text,
            'sender' => Config::SENDER,
            'timestamp' => Content::curl('https://lk.redsms.ru/get/timestamp.php'),
            'return' => 'json',
        );
        $params['signature'] = self::signature($params);
        if (Config::IS_TEST) {
            echo $text;
            return true;
        }
        $arResult = Content::curl('https://lk.redsms.ru/get/send.php?' . http_build_query($params));
        if (isset($arResult[0])) {
            $arResult = $arResult[0];
        }
        $phone = str_replace('+', '', $phone);
        return (isset($arResult[$phone]['error']) and empty($arResult[$phone]['error']));
    }

    static private function signature($params) {
        ksort($params);
        reset($params);
        return md5(implode($params) . Config::KEY);
    }

}
