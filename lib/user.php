<?php

namespace Project\Sms;

use CUser;

class User {

    static public function initCode($phone) {
        $arUser = self::getPhone(Utility::filterPhone($phone));
        if ($arUser) {
            if ($arUser['UF_PHONE_COUNT'] and $arUser['UF_PHONE_COUNT'] > Config::SMS_HOUR_MAX) {
                $arUser['SMS']['ERROR'] = 'За час можно отправить только ' . Config::SMS_HOUR_MAX . ' sms';
                return $arUser;
            }

            $arFields = array();
            if (($arUser['UF_PHONE_TIME'] + Config::HOUR_TIME) > time()) {
                $arFields['UF_PHONE_COUNT'] = $arUser['UF_PHONE_COUNT'] + 1;
            } else {
                $arFields['UF_PHONE_COUNT'] = 1;
            }
            $arFields['UF_PHONE_TIME'] = time();
            $arFields['UF_PHONE_CODE'] = randString();
            $arFields['UF_PHONE_CODE_LIMIT'] = Config::ERROR_LIMIT;
            self::update($arUser['ID'], $arFields);
            $arUser['SMS'] = array(
                'IS_SEND' => true,
                'TIME' => $arFields['UF_PHONE_TIME'],
                'LIMIT' => $arFields['UF_PHONE_CODE_LIMIT'],
                'CODE' => $arFields['UF_PHONE_CODE'],
            );
//            preDebug(__FUNCTION__, $arUser['SMS']);
        }
        return $arUser;
    }

    static public function getCode($phone) {
        $arUser = self::getPhone(Utility::filterPhone($phone));
        if ($arUser) {
            if ($arUser['UF_PHONE_CODE']) {
                $arUser['SMS'] = array(
                    'ACTIVE' => $arUser['UF_PHONE_CODE_LIMIT'] and ( $arUser['UF_PHONE_TIME'] and ( $arUser['UF_PHONE_TIME'] + Config::TIME) > time())
                );
                if ($arUser['SMS']['ACTIVE']) {
                    $arUser['SMS']['TIME'] = $arUser['UF_PHONE_TIME'];
                    $arUser['SMS']['LIMIT'] = $arUser['UF_PHONE_CODE_LIMIT'];
                    $arUser['SMS']['CODE'] = $arUser['UF_PHONE_CODE'];
                }
            }
//            preDebug(__FUNCTION__, $arUser['SMS']);
        }
        return $arUser;
    }

    static public function getPhone($phone) {
        $filter = array(
            "ACTIVE" => "Y",
            "UF_PHONE" => $phone
        );
        $arUser = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, array(
                    'SELECT' => array(
                        'UF_CAPTCHA',
                        'UF_PHONE_CODE',
                        'UF_PHONE_TIME',
                        'UF_PHONE_COUNT',
                        'UF_PHONE_CODE_LIMIT',
                        'UF_PHONE_TIME',
                        'UF_PHONE_FAIL',
                        'UF_PHONE_FAIL_ALL',
                        'UF_PHONE_FAIL_TIME',
                    )
                ))->fetch();
        if ($arUser) {
            $arUser['UF_CAPTCHA'] = ($arUser['UF_CAPTCHA'] and ($arUser['UF_CAPTCHA'] + Config::CAPTCHA_TIME) > time());
//            pre($arUser['UF_CAPTCHA']);
        }
        return $arUser;
    }

    static public function getByID($ID) {
        return CUser::GetByID($ID)->fetch();
    }

    static public function update($ID, $arFields) {
//        preDebug(__FUNCTION__, $ID, $arFields);
        $user = new CUser;
        $user->Update($ID, $arFields);
    }

    static public function succes($arUser) {
        $arFields = array();
        $arFields['UF_PHONE_CODE'] = $arFields['UF_CAPTCHA'] = $arUser['UF_PHONE_FAIL'] = '';
        self::update($arUser['ID'], $arFields);
    }

    static public function fail($arUser) {
        $arFields = array();
        if ($arUser['SMS']['LIMIT'] < 4) {
            $arFields['UF_CAPTCHA'] = time();
            pre($arFields['UF_CAPTCHA']);
        }
        $arFields['UF_PHONE_CODE_LIMIT'] = $arUser['SMS']['LIMIT'];
        $arFields['UF_PHONE_FAIL'] = $arUser['UF_PHONE_FAIL'] + 1;
        $arFields['UF_PHONE_FAIL_ALL'] = $arUser['UF_PHONE_FAIL_ALL'] + 1;
        $arFields['UF_PHONE_FAIL_TIME'] = time();
        self::update($arUser['ID'], $arFields);
        return ($arFields['UF_CAPTCHA'] and ($arFields['UF_CAPTCHA'] + Config::CAPTCHA_TIME) > time());
    }

}
