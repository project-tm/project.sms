<? if ($arResult['STEP'] == 'auth') { ?>
    <script>
        window.location.reload();
    </script>
<? } else { ?>
    <form method="post" action="" class="popup-auth-form-sms <?= $arParams['FORM_SELECT'] ?>">
        <div class="input-block">
            <? if ($arResult['STEP'] == 'phone') { ?>
                <input type="hidden" name="isPhone" value="1">
                <div class="bx-authform-label-container">Телефон</div>
                <input type="text" name="PERSONAL_PHONE" maxlength="50" placeholder="Введите телефон" required class="first-child" <? if (isset($arResult['PHONE'])) { ?>value="<?= addslashes($arResult['PHONE']) ?>"<? } ?>>
                <? if ($arResult['ERROR']) { ?>
                    <div class="error"><?= $arResult['ERROR'] ?></div>
                <? } ?>
                <script>
                    $(function () {
                        window.filterInputmask('[name="PERSONAL_PHONE"]');
                    });
                </script>
                <button type="submit" class="reg-btn">Получить код</button>
            <? } else { ?>
                <? if ($arResult["SECURE_AUTH"]): ?>
                    <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                    <noscript>
                    <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                    </span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure<?= $arResult["RND"] ?>').style.display = 'inline-block';
                    </script>
                <? endif ?>
                <div class="bx-authform-label-container">Введите код из sms</div>
                <input type="hidden" name="isSms" value="1">
                <input type="hidden" name="PERSONAL_PHONE" value="<?= addslashes($arResult['PHONE']) ?>">
                <input type="text" name="CODE" maxlength="10" placeholder="Введите код из sms" required class="first-child">
                <? if ($arResult["CAPTCHA_CODE"]): ?>
                    <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" class="captcha_sid" />
                    <input type="text" name="captcha_word" maxlength="50" value="" placeholder="Введите код с картинки" required/>
                <? endif ?>
                <? if ($arResult['ERROR']) { ?>
                    <div class="error"><?= $arResult['ERROR'] ?></div>
                <? } ?>
                <button type="submit" class="reg-btn">Войти</button>
            <? } ?>
        </div>
    </form>
<? } ?>